import QRReader from './vendor/qrscan.js';
import { snackbar } from './snackbar.js';
import styles from '../css/styles.css';
import isURL from 'is-url';
import $ from 'jquery';

//If service worker is installed, show offline usage notification
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then(reg => {
        console.log('SW registered: ', reg);
        if (!localStorage.getItem('offline')) {
          localStorage.setItem('offline', true);
        }
      })
      .catch(regError => {
        console.log('SW registration failed: ', regError);
      });
  });
}

window.addEventListener('DOMContentLoaded', () => {
  //To check the device and add iOS support
  window.iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
  window.isMediaStreamAPISupported = navigator && navigator.mediaDevices && 'enumerateDevices' in navigator.mediaDevices;
  window.noCameraPermission = false;

  var copiedText = null;
  var frame = null;
  var token;
  var selectPhotoBtn = document.querySelector('.app__select-photos');
  var dialogElement = document.querySelector('.app__dialog');
  var dialogOverlayElement = document.querySelector('.app__dialog-overlay');
  var dialogOpenBtnElement = document.querySelector('.app__dialog-open');
  var dialogCloseBtnElement = document.querySelector('.app__dialog-close');
  var scanningEle = document.querySelector('.custom-scanner');
  var textBoxEle = document.querySelector('#result');
  window.appOverlay = document.querySelector('.app__overlay');
  var logonBx = document.querySelector('#applogonBx');
  var tableBoxClose = document.querySelector('.table__dialog-close');
  var selectBx = document.querySelector('#selectBx'); 
  var selectCntr = document.querySelector('#selectCntr'); 
  var loader = document.querySelector('.loading'); 
  var loginSbmt = false;
  $(document).ready(function () {
    logonBx.style.display = 'block';
  })
  $('#selectCntr').change(function(){
    localStorage.setItem('event',$(this).find("option:selected").attr('value'))
  });
  $("#btn2").click(function () {
    selectCntr.style.display = 'none';
  })
  $("#btn1").click(function () {
    if(!loginSbmt){
      loginSbmt = true;
      loader.style.display = 'block';
      var email = $('#email').val();
      var password = $('#password').val();
      var data = {
        username: email,
        password: password
      }
      $.ajax({
        type: "POST",
        url: "https://qrappapi.herokuapp.com/api/login",
        data: data,
        dataType: "text",
        success: function (resultData) {
          var loginData = JSON.parse(resultData);
          console.log(loginData);
          localStorage.setItem("token", loginData.token);
          logonBx.style.display = 'none';
          selectDisplay(loginData.events);
          loader.style.display = 'none';
          qrCodeInit();
          snackbar.show('login success', 3000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          loader.style.display = 'none';
          loginSbmt = false;
          snackbar.show('Some Error Occured', 3000);
        }
      })
    }
  });

  function selectDisplay(event){
    localStorage.setItem('event',event[0]);
    selectCntr.style.display = 'block';
    selectBx.innerHTML='';
    for(var i = 0; i < event.length; i++) {
        var opt = event[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        selectBx.appendChild(el);
    }
  }
  function qrCodeInit(){
    QRReader.init(); //To initialize QR Scanner
    // Set camera overlay size
    setTimeout(() => {
      setCameraOverlay();
      if (window.isMediaStreamAPISupported) {
        scan();
      }
    }, 1000);
  }
  function setCameraOverlay() {
    window.appOverlay.style.borderStyle = 'solid';
  }

  function createFrame() {
    frame = document.createElement('img');
    frame.src = '';
    frame.id = 'frame';
  }

  //Dialog close btn event
  dialogCloseBtnElement.addEventListener('click', hideDialog, false);
  dialogOpenBtnElement.addEventListener('click', openInBrowser, false);
  tableBoxClose.addEventListener('click', closeTableBx);

  function closeTableBx() {
    scanningEle.style.display = 'block';
    scan();
  }
  //To open result in browser
  function openInBrowser() {
    markInSheet();
    scanningEle.style.display = 'none';
    dialogElement.classList.add('app__dialog--hide');
    dialogOverlayElement.classList.add('app__dialog--hide');
    scan();
  }
  function markInSheet(){
      loader.style.display = 'block';
      var url = "https://qrappapi.herokuapp.com/api/mark";
      var headers = { 'x-auth-token': localStorage.getItem('token') }
      var data ={
        code: copiedText,
        event: localStorage.getItem('event'),
        status:'YES'
      }
      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        headers: headers,
        dataType: "text",
        success: function (res) {
          console.log(res);
          loader.style.display = 'none';
          snackbar.show('Attendance Marked', 3000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          loader.style.display = 'none';
          snackbar.show('Some Error Occured', 3000);
        }
      });
  }

  function scan(forSelectedPhotos = false) {
    if (window.isMediaStreamAPISupported && !window.noCameraPermission) {
      scanningEle.style.display = 'block';
    }

    if (forSelectedPhotos) {
      scanningEle.style.display = 'block';
    }

    QRReader.scan(result => {
      copiedText = result;
      textBoxEle.innerHTML = '';
      loader.style.display = 'block';
      var url = "https://qrappapi.herokuapp.com/api/scancode/" + result;
      var headers = { 'x-auth-token': localStorage.getItem('token') }
      $.ajax({
        url: url,
        type: 'GET',
        headers: headers,
        dataType: 'json',
        success: function (res) {
          loader.style.display = 'none';
          console.log(res);
          textBoxEle.innerHTML = '';
          var cnfBox = document.createElement("div");
          cnfBox.setAttribute("class", "cnf_bx_text");
          var nameBox = document.createElement("div");
          var node1 = document.createTextNode("Name:" + res.user.firstname + res.user.lastname);
          nameBox.appendChild(node1);
          var amountDue = document.createElement("div");
          var node2 = document.createTextNode("Amount Due:" + res.user.amountdue);
          amountDue.appendChild(node2);
          var member = document.createElement("div");
          var node3 = document.createTextNode("Member Id:" + res.user.picesgmember);
          member.appendChild(node3);
          cnfBox.appendChild(nameBox);
          cnfBox.appendChild(amountDue);
          cnfBox.appendChild(member);
          textBoxEle.appendChild(cnfBox);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          loader.style.display = 'none';
          snackbar.show('Some Error Occured', 3000);
        }
      });

      scanningEle.style.display = 'none';
      dialogOpenBtnElement.style.display = 'inline-block';
      dialogElement.classList.remove('app__dialog--hide');
      dialogOverlayElement.classList.remove('app__dialog--hide');
      const frame = document.querySelector('#frame');

    }, forSelectedPhotos);
  }

  function hideDialog() {
    copiedText = null;
    textBoxEle.value = '';
    if (!window.isMediaStreamAPISupported) {
      frame.src = '';
      frame.className = '';
    }
    dialogElement.classList.add('app__dialog--hide');
    dialogOverlayElement.classList.add('app__dialog--hide');
    scan();
  }

  function selectFromPhoto() {
    //Creating the camera element
    var camera = document.createElement('input');
    camera.setAttribute('type', 'file');
    camera.setAttribute('capture', 'camera');
    camera.id = 'camera';
    window.appOverlay.style.borderStyle = '';
    selectPhotoBtn.style.display = 'block';
    createFrame();

    //Add the camera and img element to DOM
    var pageContentElement = document.querySelector('.app__layout-content');
    pageContentElement.appendChild(camera);
    pageContentElement.appendChild(frame);

    //Click of camera fab icon
    selectPhotoBtn.addEventListener('click', () => {
      scanningEle.style.display = 'none';
      document.querySelector('#camera').click();
    });

    //On camera change
    camera.addEventListener('change', event => {
      if (event.target && event.target.files.length > 0) {
        frame.className = 'app__overlay';
        frame.src = URL.createObjectURL(event.target.files[0]);
        if (!window.noCameraPermission) scanningEle.style.display = 'block';
        window.appOverlay.style.borderColor = 'rgb(62, 78, 184)';
        scan(true);
      }
    });
  }
});
